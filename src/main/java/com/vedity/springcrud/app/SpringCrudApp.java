package com.vedity.springcrud.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.vedity.springcrud")
@EntityScan("com.vedity.springcrud.model")
@EnableJpaRepositories("com.vedity.springcrud.repository")
public class SpringCrudApp {

	public static void main(String[] args) {
		SpringApplication.run(SpringCrudApp.class, args);
	}

}

